/*
* SJF - Shortest Job First.
* 
* For SJF, if we have the following process queue.
*
*   0        1           2
*  ----      --      ----------  
* |    | -> |  | -> |          | 
*  ----      --      ----------
*
*  We complete the shortest job first. In this case
*  1 until completion, then 0, then 2.
*/

#include "utils.h"

proc *shortestProc(queue *q);

proc *shortestProc(queue *q)
{
	proc *aux, *shortest;
	aux = q->head;
	shortest = aux;

	/* compare the size between different process */
	while (aux) {
		if (aux->size < shortest->size)
			shortest = aux;
		aux = aux->next;
	}

	return shortest;
}

void sjf(queue *q)
{
	proc *temp = shortestProc(q);
	proc **pp = &temp;

	if (!(*pp)) {
		return;
	}

	if ((*pp)->comp == 0)
		q->sum += q->runtime - (*pp)->arrival;

	addIdle(*pp);
	q->runtime += CPU_CL;

	(*pp)->comp += CPU_CL;
	if (is_RT_proc(*pp))
		(*pp)->idle = 0;
}
