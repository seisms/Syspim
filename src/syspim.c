/* Syspim - System Process sIMulator
 *
 *	Syspim handles the generation and insertion of "processes" and
 *	queue management
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "syspim.h"

void finish_processes(queue *q)
{
	proc **curr = &q->head;
	while (*curr) {
		if (is_RT_proc(*curr) && (*curr)->idle >= (*curr)->latency) {
			q->head = procElim(q->head, *curr);
			q->crashed++;
			return;
		}

		if ((*curr)->comp >= (*curr)->size) {
			q->head = procElim(q->head, *curr);
			q->completed++;
			return;
		}
		curr = &(*curr)->next;
	}
}

void systemLoop(queue *q, int choice, bool interactive_flag, bool rt_flag)
{
	bool rt, io;
	queue *m;
	/* add algorithms here as they  appear in USAGE*/
	void (*algorithm[])(queue *) = { &fcfs, &robin, &sjf, &srtf };

	if (choice >= sizeof((algorithm)) / sizeof((algorithm[0]))) {
		fprintf(stderr,
				"[ERROR] in systemLoop: Chosen algorithm does not exist.\n");
		return;
	}

	io = false;

	/* we always start with a set number of processes */
	for (int i = 0; i < INIT_PROC; i++) //INIT_PROC = 100
		queueNew(q, false);

	while (q->state) {
		/* random process creation */
		rt = false;
		if (rt_flag && rand() % 2)
			rt = true;

		if (interactive_flag && rand() % 2)
			io = !io;

		int n = rand() % 100;
		m = (io && !rt) ? &q[1] : &q[0];

		if (n <= PROC_CHANCE) //PROC_CHANCE = 2
			queueNew(m, rt);

		/* here we apply the CPU scheduling algorithm */
		(*algorithm[choice])(m);

		finish_processes(m);
		if (!q->head)
			q->state = DEAD;
	}
}

void queueInit(queue *q)
{
	q->state = RUNNING;
	q->head = NULL;
	q->sum = 0;
	q->tot = 0;
	q->crashed = 0;
	q->runtime = 0;
	q->completed = 0;
	q->rtproc = 0;
}

void queueNew(queue *q, bool rt)
{
	q->head = procInsert(q->head, rt, q->runtime);
	q->tot++;
	if (rt)
		q->rtproc++;
}

proc *procInsert(proc *h, bool interactive, unsigned int arrival)
{
	proc *new = malloc(sizeof(proc));
	if (!new)
		perror("in procInsert malloc");

	int n = 0;
	while (!n)
		n = rand() % SIZE_RANGE;

	new->size = CPU_CL *n;
	new->comp = 0;
	new->arrival = arrival;
	new->latency = (interactive) ? CPU_CL *(SIZE_RANGE * 4) : -1;
	new->idle = (interactive) ? 0 : -1;
	new->next = NULL;

	proc **curr = &h;
	while (*curr)
		curr = &(*curr)->next;
	*curr = new;
	return h;
}

proc *procElim(proc *h, proc *eli)
{
	proc **curr = &h;

	while (*curr && *curr != eli) {
		curr = &(*curr)->next;
	}

	if (*curr == eli) {
		*curr = eli->next;
		free(eli);
	}

	return h;
}
void procClean(proc *h)
{
	proc *tmp;
	while (h) {
		tmp = h;
		h = h->next;
		free(tmp);
	}
}

void queueInfo(queue q, queue q2)
{
	float avg = ((float)(q.sum + q2.sum) / (q.tot + q2.tot)) / CPU_CL;
	unsigned int total = q.tot + q2.tot;
	int real_runtime = (q.runtime + q2.runtime) / CPU_CL;
	float qcrash = ((float)(q.crashed + q2.crashed) / (q.rtproc + q2.rtproc)) * 100;

	printf("Completed: %d\n"
		   "Crashed: %d\n"
		   "Runtime: %d cycles\n"
		   "total processes: %d\n"
		   "RT processes: %d\n"
		   "average wait time: %.2f cycles\n"
		   "percentage of RT crashes: %.2f\n"
		   "throughput: %.3f\n",
		   q.completed + q2.completed, q.crashed + q2.crashed, real_runtime,
		   total, q.rtproc + q2.rtproc, avg, qcrash,
		   (float)total / real_runtime);
}

void procInfo(proc *h)
{
	proc *tmp = h;
	int i = 0;
	while (tmp) {
		printf("\t--------%d-------\n"
			   "\tsize: %d\n"
			   "\tcomp: %d\n"
			   "\tlatency: %d\n"
			   "\tidle: %d\n"
			   "\t--------------\n"
			   "\t  |\n\t  V\n",
			   i, tmp->size, tmp->comp, tmp->latency, tmp->idle);
		i++;
		tmp = tmp->next;
	}
	printf("\n");
}

void parse_arg(char *arg, bool *interactive_flag, bool *rt_flag)
{
	if (!strncmp(arg, "-R", strlen("-R")))
		*rt_flag = true;
	if (!strncmp(arg, "-I", strlen("-I")))
		*interactive_flag = true;
	/* if its a number */
	if (48 <= (int)arg[0] && (int)arg[0] <= 57) {
		srand(atoi(arg));
	} else {
		srand((unsigned)time(NULL));
	}
}

int main(int argc, char *argv[])
{
	/************** ARGUMENT HANDLING *****************/
	int choice;
	bool interactive, rt = false;
	if (argc < 2 || argc > 5) {
		puts(USAGE);
		return 0;
	} else if (argc == 2) {
		choice = atoi(argv[1]);
		srand(time(NULL));
	} else {
		choice = atoi(argv[1]);
		for (int i = 2; i < argc; i++)
			parse_arg(argv[i], &interactive, &rt);
	}
	printf("interactive? %s\n"
		   "real time? %s\n",
		   interactive ? "y" : "n", rt ? "y" : "n");
	/************************************************/

	queue q[2] = { q[0], q[1] };
	queueInit(&q[0]);
	queueInit(&q[1]);

	systemLoop(q, choice, interactive, rt);

	queueInfo(q[0], q[1]);

	procClean(q[0].head);
	procClean(q[1].head);
	return 0;
}
