#include "utils.h"

void addIdle(proc *h)
{
	proc **pp = &h;
	while (*pp) {
		if (is_RT_proc(*pp))
			(*pp)->idle += CPU_CL;
		pp = &(*pp)->next;
	}
}

int is_RT_proc(proc *eval)
{
	return (eval->latency != -1);
}
