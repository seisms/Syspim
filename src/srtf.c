/*
* SRTF - Shortest Time Remaining First
*
* We assign a process to running for n CPU clocks
* then check for a process with a shorter remaining time.
*/

#include "utils.h"

proc *shortestBurn(queue *q);

proc *shortestBurn(queue *q)
{
	proc *aux, *shortest;
	int i;
	aux = q->head;
	shortest = aux;

	/* compare the burning time between different process */
	while (aux) {
		if ((aux->size - aux->comp) < (shortest->size - shortest->comp))
			shortest = aux;
		aux = aux->next;
	}
	return shortest;
}

void srtf(queue *q)
{
	proc *temp = shortestBurn(q);
	proc **pp = &temp;

	if (!(*pp)) {
		return;
	}

	if ((*pp)->comp == 0)
		q->sum += q->runtime - (*pp)->arrival;

	addIdle(*pp);
	q->runtime += CPU_CL;

	(*pp)->comp += CPU_CL;
	if (is_RT_proc(*pp))
		(*pp)->idle = 0;
}
