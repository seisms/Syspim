#include <stdbool.h>
#include "utils.h"

#define USAGE                                   \
	"./syspim <algorithm> <options>\n"          \
	"algorithm:\n"                              \
	"\t0 - FCFS\t1 - Round Robin\n"             \
	"\t2 - SJF \t3 - SRTF\n"                    \
	"options:\n"                                \
	"\t-R enables Real time process creation\n" \
	"\t-I enables interactive process queue\n"

#define PROC_CHANCE 2
#define SIZE_RANGE 60
#define INIT_PROC 100

void queueNew(queue *q, bool interactive);
void queueInfo(queue q, queue q2);
void systemLoop(queue *q, int choice, bool interactive_flag, bool rt_flag);
void finish_processes(queue *q);

proc *procInsert(proc *h, bool interactive, unsigned int runtime);
void procClean(proc *h);
void procInfo(proc *h);

void parse_arg(char *arg, bool *interactive_flag, bool *rt_flag);
