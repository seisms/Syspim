/*	FCFS - First Come First Serve
 *
 *  For this CPU process scheduling algorithm, imagine the following process queue:
 *   
 *   0        1           2                    n
 *  ----      --      ----------              ----
 * |    | -> |  | -> |          | -> .... -> |    |
 *  ----      --      ----------              ----
 *  
 *  Where the "length" of each process is representative of the amount of CPU clocks
 *  needed to complete it.
 *
 *  When doing FCFS, we would assign 0 to running until completion, then 1 until completion
 *  then 2 until completion, and so on.
 */

#include "utils.h"
#include <stdio.h>

void fcfs(queue *q)
{
	/*	pointer to pointer for in-place modifying */
	proc **curr = &q->head;

	if (!(*curr))
		return;

	if ((*curr)->comp == 0)
		q->sum += q->runtime - (*curr)->arrival;

	addIdle(*curr);

	q->runtime += CPU_CL;
	(*curr)->comp += CPU_CL;

	if (is_RT_proc(*curr))
		(*curr)->idle = 0;
}
