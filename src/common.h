/* Variable and function declarations, along with everything
 * else associated with headers, common to two or more 
 * source (.c) files go here.
 *
 * If a function or variable declaration is exclusive to
 * a certain CPU scheduling algorithm, it should be on its
 * own header file, associated with that algorithm.
 */

#define CPU_CL 1024

typedef enum {
	DEAD,
	RUNNING,
} _state;

typedef struct proc {
	unsigned int size;
	unsigned int comp;
	unsigned int arrival;
	int latency; /* -1 if not a real time process, >= CPU_CL else */
	int idle; /* time since RT process was last attended, -1 if not RT*/
	struct proc *next;
} proc;

/* wrapper around process queue, to get some
 * important information about it
 */
typedef struct queue {
	proc *head;
	unsigned int sum;
	unsigned int tot;
	unsigned int crashed;
	unsigned int completed;
	unsigned int runtime;
	unsigned int rtproc;
	_state state;
} queue;

proc *procElim(proc *h, proc *eli);

void fcfs(queue *q);
void robin(queue *q);
void sjf(queue *q);
void srtf(queue *q);
