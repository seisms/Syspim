/*
* Round Robin
*
* Much like FCFS, but instead of running a process until
* completion, we go linearly through the queue, completing
* n clocks of each process.
*/

#include "utils.h"
#include <stdlib.h>

proc *find_to_sum(proc *h);

proc *find_to_sum(proc *h)
{
	if(!h)
		return NULL;
	unsigned int prev_comp = h->comp;
	proc *tmp = h;

	while (tmp) {
		if (tmp->comp < prev_comp)
			return tmp;
		prev_comp = tmp->comp;
		tmp = tmp->next;
	}
	return h;
}

void robin(queue *q)
{
	proc *tmp = find_to_sum(q->head);
	proc **curr = &tmp;

	if (!(*curr))
		return;

	if ((*curr)->comp == 0)
		q->sum += q->runtime - (*curr)->arrival;

	addIdle(q->head);
	q->runtime += CPU_CL;

	(*curr)->comp += CPU_CL;
	if (is_RT_proc(*curr))
		(*curr)->idle = 0;
}
