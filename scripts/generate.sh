#!/usr/bin/env bash

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
NC='\033[0m'
DIR='data/'

BAR_CHAR_DONE="#"
BAR_CHAR_TODO="-"
BAR_PERCENTAGE_SCALE=2
BAR_SIZE=40
SAMPLES=1000


function show_progress {
	CURRENT="$1"
	TOTAL="$2"

	# calculate the progress in percentage 
	PERCENT=$(bc <<< "scale=$BAR_PERCENTAGE_SCALE; 100 * $CURRENT / $TOTAL" )
	# The number of done and todo characters
DONE=$(bc <<< "scale=0; $BAR_SIZE * $PERCENT / 100" )
TODO=$(bc <<< "scale=0; $BAR_SIZE - $DONE" )

	# build the done and todo sub-bars
	DONE_SUB_BAR=$(printf "%${DONE}s" | tr " " "${BAR_CHAR_DONE}")
	TODO_SUB_BAR=$(printf "%${TODO}s" | tr " " "${BAR_CHAR_TODO}")

	# output the bar
	echo -ne "\r\t${BLUE}Progress : ${NC}[${DONE_SUB_BAR}${TODO_SUB_BAR}] ${PERCENT}%"
}

function get_info() {
	build/syspim $1 $2 $3 $(date +%s%N)| awk '/total/ {printf $3 "\t"} /Runtime/ {printf $2 "\t"} /average wait time/ {printf $4 "\t"} /percentage of RT crashes/ {printf $5 "\t"} /throughput/ {print $2}'
}

function buildplot() {
	gnuplot -e "filename='$1'" -e "outputname='$2'" $3
}

function make_data() {
	echo -e "${RED}INFO: ${NC}Testing with $SAMPLES samples and $1 $2 flags"
	for ((j = 0; j < 4; j++)); do
		echo -e "${BLUE}Building data for algorithm ${RED}$j..."
		for ((i = 0; i < 1000; i++)); do
			if [ "$j" = "0" ]; then
				FILE="fcfs.dat"
			fi

			if [ "$j" = "1" ]; then
				FILE="robin.dat"
			fi

			if [ "$j" = "2" ]; then
				FILE="sjf.dat"
			fi

			if [ "$j" = "3" ]; then
				FILE="strn.dat"
			fi
			if [ "$i" = "0" ]; then
				echo -e "RUNTIME\tTOTAL\tAVG_WAIT\tRTCRASH%\tTHROUGHPUT" >> "$DIR$1_$2$FILE"
			fi
			show_progress $i 1000
			get_info $j $1 $2 >> "$DIR$1_$2$FILE"
		done
		echo -e " ${GREEN}Done!"
	done
}
echo -e "${RED}INFO: ${NC}Creating folders and deleting previous *.dat..."
if [ ! -d $DIR ]; then
	mkdir "$DIR"
fi

if [ ! -d imgs ]; then
	mkdir imgs
fi

if [ ! -d imgs/throughput ]; then
	mkdir imgs/throughput
fi

if [ ! -d imgs/wait_time ]; then
	mkdir imgs/wait_time
fi

rm $DIR*.dat
echo -e "${GREEN}Done!"
make_data
make_data -R
make_data -R -I
echo -e "${RED}INFO: ${NC}Converting .dat files to png plots..."
echo -e "${BLUE}Converting throughput data to png..."
for file in data/*.dat; do
	filename=$(echo "$file" | sed "s/\..*//" | sed 's/data\///')
	buildplot "$file" "imgs/throughput/$filename.png" scripts/throughput.plg
done
echo -e "${GREEN} Done!"

echo -e "${BLUE}Converting wait time data to png..."
for file in data/*.dat; do
	filename=$(echo "$file" | sed "s/\..*//" | sed 's/data\///')
	buildplot "$file" "imgs/wait_time/$filename.png" scripts/wait.plg
done

echo -e "${GREEN} Done!"
