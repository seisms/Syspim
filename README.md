# Syspim - System Process sIMulator

Small program to (in a very rudimentary way) evaluate performance of different CPU scheduling algorithms under different conditions.

Constants in `src/syspim.h` and `src/common.h` control program behaviour. Exercise care when changing constants related to process generation, as it could lead to infinite process creation if the values are set too high, and therefore an infinite loop.

To compile `make` in command line. To remove executable, object files, plot data and images: `make clean`. Run binary with no arguments to print usage.

# Dependencies

- (Optional) Gnuplot is used to plot wait time/throughput data of each algorithm.
