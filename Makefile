CC = gcc
SRCS = $(wildcard src/*.c)
HEADERS = $(wildcard src/*.h)
OBJS = $(SRCS:.c=.o)
FLAGS = -O2 -Wall

syspim: $(OBJS) $(HEADERS)
	$(CC) -o build/syspim $(OBJS) $(FLAGS)

debug:
	$(CC) -o build/debug $(SRCS) -g

clean :
	rm $(OBJS) build/syspim build/debug imgs/throughput/* imgs/wait_time/* data/*
